include device/rockchip/rk3399/BoardConfig.mk


TARGET_BOARD_PLATFORM_PRODUCT := box

TARGET_BASE_PARAMETER_IMAGE := device/rockchip/common/baseparameter/baseparameter_fb720.img

PRODUCT_PACKAGE_OVERLAYS += device/rockchip/rk3399/rk3399_box/overlay

#disable gms
BUILD_WITH_GOOGLE_GMS_EXPRESS := false

#box hidl
DEVICE_MANIFEST_FILE := $(TARGET_DEVICE_DIR)/manifest.xml

#disable sensor
BOARD_COMPASS_SENSOR_SUPPORT = false
BOARD_GYROSCOPE_SENSOR_SUPPORT = false
BOARD_PROXIMITY_SENSOR_SUPPORT = false
BOARD_SENSOR_ST := false
BOARD_SENSOR_MPU_PAD := false
BOARD_GRAVITY_SENSOR_SUPPORT := false
BOARD_LIGHT_SENSOR_SUPPORT := false
